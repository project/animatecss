/**
 * @file
 * Contains definition of the behaviour Animate.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  const compat = drupalSettings.animatecss.compat;

  Drupal.behaviors.animateCSS = {
    attach: function (context, settings) {

      const elements = drupalSettings.animatecss.elements;

      // Iterate through each element and apply animations.
      $.each(elements, function (index, element) {
        let options = {
          selector: element.selector,
          animation: element.animation,
          delay: element.delay,
          time: Number(element.time),
          speed: element.speed,
          duration: Number(element.duration),
          repeat: element.repeat,
          clean: element.clean,
          event: element.event,
          once: element.once,
          display: element.display // Adding the display option
        };

        // If the selector is an array, apply animations to each selector.
        if (Array.isArray(options.selector)) {
          $.each(options.selector, function (index, selector) {
            if (once('animatecss', selector, context).length) {
              options.selector = selector;
              let animateCSS = new Drupal.animateCSS(options);
            }
          });
        } else {
          // Apply animation to the single selector.
          if (once('animatecss', options.selector, context).length) {
            let animateCSS = new Drupal.animateCSS(options);
          }
        }
      });
    }
  };

  /**
   * Applies Animate.css classes and styles based on provided options.
   *
   * @param {object} options
   *   Configuration options for the animation.
   * @param {string|string[]} options.selector
   *   The CSS selector(s) of the element(s) to animate.
   * @param {string} options.animation
   *   The name of the animation to apply.
   * @param {string} [options.delay]
   *   The delay class or 'custom' to set a custom delay.
   * @param {number} [options.time]
   *   The custom delay time in milliseconds.
   * @param {string} [options.speed]
   *   The speed class or 'custom' to set a custom duration.
   * @param {number} [options.duration]
   *   The custom duration time in milliseconds.
   * @param {string} [options.repeat]
   *   The repeat class.
   * @param {boolean} [options.clean]
   *   The clean for remove previous class.
   * @param {string} [options.event]
   *   The event type to trigger the animation (click, mouseover, etc.).
   */
  Drupal.animateCSS = function (options) {
    // Determine the appropriate prefix based on compatibility settings.
    let Prefix = compat ? '' : 'animate__';
    let Classes = `${Prefix}animated ${Prefix}${options.animation}`;

    if (options.delay && options.delay != 'custom') {
      Classes += ` ${Prefix}${options.delay}`;
    }
    if (options.speed && options.speed != 'custom' && options.speed != 'medium') {
      Classes += ` ${Prefix}${options.speed}`;
    }
    if (options.repeat && options.repeat != 'repeat-1') {
      Classes += ` ${Prefix}${options.repeat}`;
    }

    const applyAnimation = function () {
      // Remove previous animation classes if 'clean' is true.
      if (options.clean) {
        $(options.selector).removeClass(function (index, className) {
          return (className.match(/(^|\s)animate__\S+/g) || []).join(' ');
        });
      }

      // Add custom animation delay if specified.
      if (options.delay == 'custom' && typeof options.time === 'number' && options.time > 0) {
        $(options.selector).css({
          '-webkit-animation-delay': `${options.time}ms`,
          '-moz-animation-delay': `${options.time}ms`,
          '-ms-animation-delay': `${options.time}ms`,
          '-o-animation-delay': `${options.time}ms`,
          'animation-delay': `${options.time}ms`,
          '--animate-delay': `${options.time}ms`,
        });
      }

      // Add custom animation duration if specified.
      if (options.speed == 'custom' && typeof options.duration === 'number' && options.duration > 0) {
        $(options.selector).css({
          '-webkit-animation-duration': `${options.duration}ms`,
          '-moz-animation-duration': `${options.duration}ms`,
          '-ms-animation-duration': `${options.duration}ms`,
          '-o-animation-duration': `${options.duration}ms`,
          'animation-duration': `${options.duration}ms`,
          '--animate-duration': `${options.duration}ms`,
        });
      }

      // Check and change display property if required.
      if (options.display) {
        var displayValue = $(options.selector).css('display');
        if (displayValue === 'inline') {
          $(options.selector).css('display', 'inline-block');
        }
      }

      // Add Animate.css classes to the element.
      $(options.selector).addClass(Classes);

      // Remove classes and reset after animation ends to allow re-triggering.
      if (!options.once && options.event !== 'load' && options.event !== '') {
        $(options.selector).one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function () {
          $(this).removeClass(Classes);
          if (options.delay == 'custom') {
            $(this).css('animation-delay', '');
          }
          if (options.speed == 'custom') {
            $(this).css('animation-duration', '');
          }
        });
      }
    };

    // List of all supported events.
    const events = [
      'click', 'mouseover', 'mouseout', 'mousedown', 'mouseup', 'mouseenter',
      'mouseleave', 'mousemove', 'focus', 'blur', 'submit', 'resize',
      'keypress', 'keydown', 'keyup', 'dblclick', 'touchstart', 'touchend', 'scroll'
    ];

    if (events.includes(options.event)) {
      // Attach event listener to the selector for the specified event.
      if (options.event === 'scroll') {
        $(window).on('scroll', applyAnimation);
      } else {
        $(options.selector).on(options.event, applyAnimation);
      }
    } else {
      // Apply immediately if no valid event is specified.
      applyAnimation();
    }
  };

})(jQuery, Drupal, drupalSettings, once);
