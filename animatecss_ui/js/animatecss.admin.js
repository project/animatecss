/**
 * @file
 * Contains definition of the behaviour Animate.css.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  Drupal.behaviors.animateCSS = {
    attach: function (context, settings) {

      const example = drupalSettings.animatecss.sample;
      const Selector = example.selector;

      if (once('animate__sample', Selector).length) {
        let options = {
          selector: Selector,
          animation: example.animation,
          delay: example.delay,
          time: example.time,
          speed: example.speed,
          duration: example.duration,
          repeat: example.repeat,
          event: example.event,
          once: example.once,
        };
        let demoAnimateCSS = new Drupal.animateCSSdemo(options);

        // Animate.css preview replay.
        $(once('animate__replay', '.animate__replay', context)).on(
          'click',
          function (event) {
            $(Selector).attr('class', 'animate__sample');

            let options = {
              selector: Selector,
              animation: Drupal.animateGetValue('#edit-animation', '.animate__animation', example.animation),
              delay: Drupal.animateGetValue('#edit-delay', '.animate__delay', example.delay),
              time: Drupal.animateGetValue('#edit-time', '.animate__time', example.time),
              speed: Drupal.animateGetValue('#edit-speed', '.animate__speed', example.speed),
              duration: Drupal.animateGetValue('#edit-duration', '.animate__duration', example.duration),
              repeat: Drupal.animateGetValue('#edit-repeat', '.animate__repeat', example.repeat),
              event: Drupal.animateGetValue('#edit-event', '.animate__event', example.event),
              once: $('.animate__once').is(':checked'),
            };

            setTimeout(function () {
              let demoAnimateCSS = new Drupal.animateCSSdemo(options);
            }, 10);

            event.preventDefault();
          }
        );

        // Animate.css preview replay.
        $(once('animate__animation', '.animate__animation', context)).on(
          'change',
          function (event) {
            setTimeout(function () {
              $('.animate__replay').trigger('click');
            }, 10);
          }
        );

        if ( once('animate__scroll', '.animate__scroll').length ) {
          let scrollLibraries = $('.animate__scroll');

          $(window).on('load', function (e) {
            $(scrollLibraries).each(function () {
              if ($(this).is(':checked')) {
                $('#edit-event-options').removeAttr('open');
                $('#edit-event').val("load").change();
                scrollLibraries.not(this).prop({ disabled: true, checked: false });
                scrollLibraries.not(this).closest('.form-wrapper').removeAttr('open');
              }
            });
          });

          scrollLibraries.bind({
            click: function (e) {
              let currentLibrary = $(this)
                , closestWrapper = currentLibrary.closest('.form-wrapper');

              if ( currentLibrary.is(':checked') ) {
                $('#edit-event-options').removeAttr('open');
                $('#edit-event').val("load").change();
                scrollLibraries.not(this).prop({ disabled: true, checked: false });
                scrollLibraries.not(this).closest('.form-wrapper').removeAttr('open');
              } else {
                $('#edit-event-options').attr('open', 'open');
                scrollLibraries.not(this).prop({ disabled: false, checked: false });
                scrollLibraries.not(this).closest('.form-wrapper').attr('open', 'open');
              }
            },
          });
        }

      }

      once('ginEditForm', '.region-content form', context).forEach(form => {
        const sticky = context.querySelector('.gin-sticky');
        const newParent = context.querySelector('.region-sticky__items__inner');

        if (sticky !== null && newParent && newParent.querySelectorAll('.gin-sticky').length === 0) {
          newParent.appendChild(sticky);

          // Attach form elements to main form
          const actionButtons = newParent.querySelectorAll('button, input, select, textarea');

          if (actionButtons.length > 0) {
            actionButtons.forEach((el) => {
              el.setAttribute('form', form.getAttribute('id'));
              el.setAttribute('id', el.getAttribute('id') + '--gin-edit-form');
            });
          }
        }
      });

    }
  };

  /**
   * Applies Animate.css classes and styles based on provided options.
   *
   * @param {object} options
   *   Configuration options for the animation.
   * @param {string} options.selector
   *   The CSS selector of the element to animate.
   * @param {string} options.animation
   *   The name of the animation to apply.
   * @param {string} [options.delay]
   *   The delay class or 'custom' to set a custom delay.
   * @param {string} [options.speed]
   *   The speed class or 'custom' to set a custom duration.
   * @param {string} [options.repeat]
   *   The repeat class.
   * @param {number} [options.time]
   *   The custom delay time in milliseconds.
   * @param {number} [options.duration]
   *   The custom duration time in milliseconds.
   * @param {string} [options.event]
   *   The event type to trigger the animation (click, mouseover, etc.).
   */
  Drupal.animateCSSdemo = function (options) {
    // Build Animate.css classes from global AdminCSS settings.
    let Classes = `animate__animated`;

    if (options.animation) {
      Classes += ` animate__${options.animation}`;

      if (options.delay && options.delay != 'custom') {
        Classes += ` animate__${options.delay}`;
      }
      if (options.speed && options.speed != 'custom' && options.speed != 'medium') {
        Classes += ` animate__${options.speed}`;
      }
      if (options.repeat && options.repeat != 'repeat-1') {
        Classes += ` animate__${options.repeat}`;
      }

      // Add Animate.css custom properties.
      if (options.delay == 'custom') {
        $(options.selector).css({
          '-webkit-animation-delay': options.time + 'ms',
          '-moz-animation-delay': options.time + 'ms',
          '-ms-animation-delay': options.time + 'ms',
          '-o-animation-delay': options.time + 'ms',
          'animation-delay': options.time + 'ms',
          '--animate-delay': options.time + 'ms',
        });
      }
      if (options.speed == 'custom') {
        $(options.selector).css({
          '-webkit-animation-duration': options.duration + 'ms',
          '-moz-animation-duration': options.duration + 'ms',
          '-ms-animation-duration': options.duration + 'ms',
          '-o-animation-duration': options.duration + 'ms',
          'animation-duration': options.duration + 'ms',
          '--animate-duration': options.duration + 'ms',
        });
      }

      const applyAnimation = function () {
        // Add Animate.css classes to the element.
        $(options.selector).addClass(Classes);

        // Remove classes and reset after animation ends to allow re-triggering.
        if (options.once !== true && options.event !== 'load' && options.event !== '') {
          $(options.selector).one('animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd', function () {
            $(this).removeClass(Classes);
            if (options.delay == 'custom') {
              $(this).css('animation-delay', '');
            }
            if (options.speed == 'custom') {
              $(this).css('animation-duration', '');
            }
          });
        }
      };

      // List of all supported events.
      const events = [
        'click', 'mouseover', 'mouseout', 'mousedown', 'mouseup', 'mouseenter',
        'mouseleave', 'mousemove', 'focus', 'blur', 'submit', 'resize',
        'keypress', 'keydown', 'keyup', 'dblclick', 'touchstart', 'touchend', 'scroll'
      ];

      if (events.includes(options.event)) {
        // Attach event listener to the selector for the specified event.
        if (options.event === 'scroll') {
          $(window).on('scroll', applyAnimation);
        } else {
          $(options.selector).on(options.event, applyAnimation);
        }
      } else {
        // Apply immediately if no valid event is specified.
        applyAnimation();
      }
    }
  };

  /**
   * Returns the value from either the primary selector,
   * secondary selector, or a default value.
   *
   * @param {string} primarySelector
   *   Primary element selector.
   * @param {string} secondarySelector
   *   Secondary element selector.
   * @param {string} defaultValue
   *   Default value if neither selector has a value.
   *
   * @return {string}
   *   The full URL.
   */
  Drupal.animateGetValue = function (primarySelector, secondarySelector, defaultValue) {
    return $(primarySelector).val() || $(secondarySelector).val() || defaultValue;
  }

})(jQuery, Drupal, drupalSettings, once);
